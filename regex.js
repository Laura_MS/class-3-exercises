var regex2 = RegExp('\S*@\S*.\S*','g');
var str1 = 'foo@bar.baz';
var str2 = 'foo_foo@bar.bar.baz';
var str3 = '___@bar.baz';

document.write(regex2.test(str1)+'<br/>');
document.write(regex2.test(str2)+'<br/>');
document.write(regex2.test(str3)+'<br/>');